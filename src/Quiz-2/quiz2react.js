import React from 'react';
import './styling.css'

const data = [  {name: "John", age: 25, gender: "Male", profession: "Engineer", photo: "https://media.istockphoto.com/photos/portarit-of-a-handsome-older-man-sitting-on-a-sofa-picture-id1210237745"}, 
                {name: "Sarah", age: 22, gender: "Female", profession: "Designer", photo: "https://cdn.pixabay.com/photo/2018/01/15/07/51/woman-3083378_960_720.jpg"}, 
                {name: "David", age: 30, gender: "Male", profession: "Programmer", photo: "https://media.istockphoto.com/photos/handsome-mexican-hipster-man-sending-email-with-laptop-picture-id1182472756"}, 
                {name: "Kate", age: 27, gender: "Female", profession: "Model", photo: "https://cdn.pixabay.com/photo/2015/05/17/20/07/fashion-771505_960_720.jpg" }
            ]

class DataProcess extends React.Component {
    render() {
        return (
            <>
                {data.map(el=> {
                    return (
                        <div class ="boxcontent"> 
                            <img src ={el.photo} width ="200" height="120"/>
                            <p class = "name">{el.name}</p>
                            <p class = "data">{el.profession}</p>
                            <p class = "data">{el.age} years old</p>
                        </div>
                    )
                })}
            </>
        )
    }
}

export default class Photobox extends React.Component{
    render(){
        return (
            <div class="content"> 
                <DataProcess/>
            </div>
        )
    }
}