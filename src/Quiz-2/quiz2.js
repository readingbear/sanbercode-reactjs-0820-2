class BagunDatar {
    constructor(nama){
        this.Nama=nama
    }

    luas(){
        return ""
    }

    keliling(){
        return ""
    }
}

class Lingkaran extends BagunDatar {
    constructor(nama){
        super(nama)
    }
}

class Persegi extends BagunDatar {
    constructor(nama){
        super(nama)
    }
}

function filterBookPromise(colorfull,amountOfPage) {
    return new Promise(function(resolve,reject) {
        var books = [
            {name: "sinchan", totalpage: 50, isColorful: true},
            {name: "Kalkulus", totalpage: 250, isColorful: false},
            {name: "doraemon", totalpage: 40, isColorful: true},
            {name: "algoritma", totalpage: 300, isColorful: false}
        ]
        if (amountOfPage > 0){
            resolve(books.filter(x=> x.totalpage >= amountOfPage && x.isColorful == colorfull))
        }
        else {
            var reason = new Error("maaf parameter salah")
            reject(reason)
        }
    })
}

console.log(filterBookPromise(true,5))

